set autoread
set autoindent
set clipboard=unnamedplus
set completeopt=menuone,menu,longest
set cursorlineopt=line,number
set encoding=utf-8
set expandtab 
set fileformats=unix,dos
set foldlevelstart=20
set foldmethod=syntax
set foldnestmax=50
set history=100
set hlsearch
set incsearch
set listchars=eol:,space:,tab:>\ 
set mouse=a
set noautochdir
set nofoldenable
set number relativenumber
set nowrap
set nowrapscan
set shell=/bin/bash
set smartcase
set smartindent
set smarttab
set splitbelow
set splitright
set sessionoptions=blank,buffers,curdir,folds,help,tabpages,winsize,terminal,options,localoptions
set tabpagemax=200
" TEXT FORMATTING
set shiftwidth=2 
set tabstop=2
set softtabstop=0 
set noexpandtab
set guioptions+=a " enable clipboard

set tags=./tags,tags
set wildmenu
set wildmode=lastused,list,full
set updatetime=150

set showtabline=0

" Status bar configuration
set laststatus=2
set statusline=%#StatusLineFileFlags#%y
set statusline+=\ %#StatusLineCurrentBranch#%{fugitive#Head()}
set statusline+=\ %#StatusLineFileName#%f
set statusline+=\ %=
set statusline+=\ %#StatusLineCurrentTag#%{tagbar#currenttag('%s','')}
set statusline+=\ %#StatusLineNotSavedFlag#%m
set statusline+=%#StatusLineROInfo#%r
set statusline+=%#StatusLineByteInfo#<%l:%c\ %b\ %o>
set statusline+=\ %#StatusLineLineInfo#<%l\/%L\ (%p%%)>
set statusline+=\ %#StatusLineTabInfo#<%{tabpagenr()}\/%{tabpagenr('$')}>

""" FOR coc-vim plugin """
""""""""""""""""""""""""""
" TextEdit might fail if hidden is not set.
set nohidden
" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup
" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300
" Don't pass messages to |ins-completion-menu|.
set shortmess+=c
" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
set signcolumn=yes
""""""""""""""""""""""""""

set termwinkey=<M-w>

syntax on
filetype plugin on
