syntax match PUML_START               /@startuml/
syntax match PUML_END                 /@enduml/
syntax match PUML_KEYWORD             /^ *class \|^ *interface \|^ *component \|^ *package /
syntax match PUML_KEYWORD_AS          / as /
syntax match PUML_KEYWORD_DIRECTION   / direction *$/

highlight PUML_START                  cterm=italic ctermfg=5 
highlight PUML_END                    cterm=italic ctermfg=5 
highlight PUML_KEYWORD                cterm=none ctermfg=76
highlight PUML_KEYWORD_AS             cterm=none ctermfg=190
highlight PUML_KEYWORD_DIRECTION      cterm=none ctermfg=190


syntax region PUML_DIAGRAM start="@startuml" end="@enduml" fold transparent skipwhite
syntax region PUML_BLOCK start="{" end="}" fold transparent skipwhite
