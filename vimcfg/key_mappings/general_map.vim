source ~/.vim/usercfg/key_mappings/git_map.vim

" Fn-keys
map <F1>1 :tabnew ~/.vimrc<CR>
map <F1>2 :tabnew ./.vimrc<CR>
nmap <F2> "zyiw:execute "tag ".@z.""<CR>zt
nmap <F3> <C-t>
nmap <Leader><F2> :tnext<CR>
nmap <Leader><F3> :tprevious<CR>
map <F4> :SetupProject<CR>
map <F5> :so ~/.vimrc<CR>
map <F6> :so ./.vimrc<CR>
map <F7> :cp<CR>
map <F8> :cn<CR> 
map <S-F7> :cfirst<CR>
map <S-F8> :clast<CR> 
map <C-F7> :colder<CR>
map <C-F8> :cnewer<CR> 
nmap gp :bprevious<CR>
nmap gn :bnext<CR>

" Tab in insertion mode
xmap <Tab> >gv
xmap <S-Tab> <gv

" Shift key
nmap <space>l :TagbarToggle<CR>
nmap <space>T :tabs<CR>
nmap \|E 10<C-e>
nmap \|Y 10<C-y>
nmap <C-w>t :tab split<CR>
nmap <C-w>T :-tab split<CR>

" Control key
nmap <C-w>n :tabnew<CR>
nmap <C-w>N :-tabnew<CR>
nmap <C-w>O :tabo<CR>
nmap <C-w>C :tabclose<CR>
nmap <C-w>/ <C-w>\|<C-w>_

nmap <C-PageUp> :tabmove -1<CR>
nmap <C-PageDown> :tabmove +1<CR>
nmap <space>, :let @p=expand("%:p")<CR>:tabp<CR>:vert new <C-r>p<CR>
nmap <space>. :let @p=expand("%:p")<CR>:tabn<CR>:vert new <C-r>p<CR>
" Mouse wheel 
nmap <ScrollWheelDown> <Down>
nmap <C-ScrollWheelDown> 10j
nmap <S-ScrollWheelDown> <Down>
nmap <ScrollWheelUp> <Up>
nmap <C-ScrollWheelUp> 10k
nmap <S-ScrollWheelUp> <Up>
imap <ScrollWheelDown> <nop>
imap <C-ScrollWheelDown> <nop>
imap <S-ScrollWheelDown> <nop>
imap <ScrollWheelUp> <nop>
imap <C-ScrollWheelUp> <nop>
imap <S-ScrollWheelUp> <nop>
vmap <ScrollWheelDown> <nop>
vmap <C-ScrollWheelDown> <nop>
vmap <S-ScrollWheelDown> <nop>
vmap <ScrollWheelUp> <nop>
vmap <C-ScrollWheelUp> <nop>
vmap <S-ScrollWheelUp> <nop>

nmap - :let @/ = ""<CR>

" Leader remaps
" Count last search result
nmap <Leader>cls :%s///gn<CR><ESC>
" Get filename
" if has('win32')
" nmap <Leader>fn :let @*=substitute(expand("%"), "/", "\\", "g")<CR>
" nmap <Leader>ff :let @*=substitute(expand("%:p"), "/", "\\", "g")<CR>

" " This will copy the path in 8.3 short format, for DOS and Windows 9x
" nmap <Leader>fp :let @*=substitute(expand("%:p:8"), "/", "\\", "g")<CR>
" endif
nmap <Leader>cfd   :let @*=expand("%:p:h")<CR>:let @+=expand("%:p:h")<CR><ESC>
nmap <Leader>cfn   :let @*=expand("%:t")<CR>:let @+=expand("%:t")<CR><ESC>
nmap <Leader>cfnr  :let @*=expand("%:r")<CR>:let @+=expand("%:r")<CR><ESC>
nmap <Leader>cff   :let @*=expand("%:p")<CR>:let @+=expand("%:p")<CR><ESC>
nmap <Leader>fd    :echo expand('%:p:h')<CR>
nmap <Leader>ff    :echo expand('%:p')<CR>
nmap <Leader>fn    :echo expand('%:t')<CR>
nmap <Leader>fnr   :echo expand("%:r")<CR>

" View current file in hex mode
nmap <Leader>hex :!xxd %<CR>
nmap <Leader>z :set cursorline! cursorcolumn!<CR><ESC>
nmap <Leader>l :set cursorline!<CR><ESC>
nmap <space>L :windo set scrollbind!<CR>

" Toggle wrap
nmap <Leader>W :setlocal wrap!<CR><ESC>
" Toggle numbers
nmap <Leader>N :setlocal number! relativenumber!<CR><ESC>

nmap <Leader>tc :call ToggleFileFormat()<CR>:set list!<CR>:e<CR>

nmap <Leader>rf :RecentFiles<CR>
nmap <space>fg :GFiles<CR>
nmap <space>ff :Files<CR>
nmap <space>t :Tags<CR>
nmap <space>w :Windows<CR>
nmap <space>b :Buffers<CR>
nmap <space>fl :BLines<CR>
nmap <space>cc :BCommits<CR>

" COC-VIM settings 
nmap <silent> [; <Plug>(coc-diagnostic-prev)
nmap <silent> ]; <Plug>(coc-diagnostic-next)
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gs :CocCommand clangd.switchSourceHeader<CR>
nmap <silent> gr <Plug>(coc-references)

" Use escape to escape .... from terminal inside vim!
tmap <Esc> <C-\><C-n>

nmap mj :move .+1<CR>
nmap mk :move .-2<CR>
vmap mj :move '>+1<CR>gv=gv
vmap mk :move '<-2<CR>gv=gv

map <C-j> 5j
map <C-k> 5k
map <C-l> 5l
map <C-h> 5h

xmap <space>S :sort<CR>
xmap <space>s "sy:let @/=@s<CR>/<CR>
xmap <space>p "py:vimgrep /<c-r>p/g ./**<CR>
xmap <space>o "oy:vimgrep /<c-r>p/g ./**/*.

nmap <space>s :call Switch()<CR>

nmap <space><Enter> :set foldmethod=syntax<CR>za
