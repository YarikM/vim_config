import json
import subprocess
import codecs
from os import listdir
from os.path import isdir,join,isfile


coc_config_file = 'coc-settings.json'

config_json = """ {
    "tsserver.enable": true,
    "tsserver.enableJavascript": true,
    "javascript.referencesCodeLens.enable": true,
    "codeLens.enable": true,
    "coc.preferences.extensionUpdateCheck": "never",
    "coc.preferences.messageLevel": "error",
    "java.enabled": true,
    "clangd.enabled": true,
    "jedi.enable": true
}"""

config_data = json.loads(config_json)

include_paths = [ "/usr/include", "/usr/local/include" ]

clang_lib_path = '/usr/lib/clang'
clang_version_dir = [ d for d in listdir(clang_lib_path) if isdir(join(clang_lib_path, d)) ][0]
include_paths.append(join(clang_lib_path,clang_version_dir,'include'))

clangd_path = [ f for f in listdir('/usr/bin') if isfile(join('/usr/bin', f)) and 'clangd' in join('/usr/bin', f) ][0]
config_data["clangd.path"] = join('/usr/bin',clangd_path)
config_data["clangd.arguments"] = [ "--log=error", "--background-index", "--all-scopes-completion", "--limit-results=0", "--clang-tidy", "--completion-style=detailed" ]

result = subprocess.run(['which', 'npm'], stdout=subprocess.PIPE)
npm_path = result.stdout.decode('UTF-8').strip('\n')
config_data["tsserver.npm"] = npm_path

jvm_path = '/usr/lib/jvm'
java_home = [ d for d in listdir(jvm_path) if isdir(join(jvm_path, d)) and 'jdk' in join(jvm_path, d)][0]
config_data["java.home"] = join(jvm_path, java_home)

with codecs.open(coc_config_file, 'w', 'utf8') as f:
    f.write(json.dumps(config_data, sort_keys = True, ensure_ascii=False))
