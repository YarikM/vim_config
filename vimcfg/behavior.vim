" autocmd BufNewFile,BufRead SmartDeviceLinkCore.log setlocal syntax=SDLCoreLog foldmethod=marker foldmarker=Enter,Exit readonly
autocmd! BufNewFile,BufRead *.puml setlocal syntax=puml 

" Syntax folding
" autocmd! BufNewFile,BufRead,BufEnter *.js,*.c,*.cc,*.h,*.hpp setlocal foldmethod=syntax <<<<----- SLOWS DOWN VIM!!!
autocmd! BufNewFile,BufRead,BufEnter *.py,*.lua setlocal foldmethod=indent
" Flog -> get commit from current line
autocmd! BufNewFile,BufRead,BufEnter flog* nmap yc :execute 'normal! ^%%l"+yw"*yw'<CR>
autocmd! BufNewFile,BufRead,BufEnter fugitive* setlocal foldmethod=diff

" Tag list function defs helper
autocmd! FileType lua setlocal iskeyword+=:

autocmd! BufNewFile,BufRead,BufEnter *.txt setlocal foldmethod=manual
