function! IsValid()
  silent !clear
  let fileext = expand("%:e")
  if (fileext == "xml")
    !xmllint --noout %
  endif
  silent redraw!
endfunction

function! Validate()
  silent !clear
  let fileext = expand("%:e")
  if (fileext == "xml")
    %!xmllint %
  endif
  silent redraw!
endfunction

function! Format()
  silent !clear
  let fileext = expand("%:e")
  if (fileext == "xml")
    %!xmllint --huge --format %
  elseif (fileext == "json")
    %!python -m json.tool
  elseif (fileext == "js")
    call JsBeautify()
  endif
  silent redraw!
endfunction
 
function! ToggleFileFormat()
  if (&fileformats == 'unix')
    set fileformats=unix,dos
  else
    set fileformats=unix
  endif
endfunction
    
function! HighlightLine(color)
  if (a:color == 'Red')
    call matchadd('HighlightRed', '\%'.line('.').'l')
  elseif (a:color == 'Yellow')
    call matchadd('HighlightYel', '\%'.line('.').'l')
  elseif (a:color == 'Cyan')
    call matchadd('HighlightCya', '\%'.line('.').'l')
  elseif (a:color == 'Green')
    call matchadd('HighlightGre', '\%'.line('.').'l')
  elseif (a:color == 'Purple')
    call matchadd('HighlightPur', '\%'.line('.').'l')
  elseif (a:color == 'White')
    call matchadd('HighlightWhi', '\%'.line('.').'l')
  endif
endfunction

function! HighlightColumn(color)
  if (a:color == 'Red')
    call matchadd('HighlightRed', '\%'.col('.').'v')
  elseif (a:color == 'Yellow')
    call matchadd('HighlightYel', '\%'.col('.').'v')
  elseif (a:color == 'Cyan')
    call matchadd('HighlightCya', '\%'.col('.').'v')
  elseif (a:color == 'Green')
    call matchadd('HighlightGre', '\%'.col('.').'v')
  elseif (a:color == 'Purple')
    call matchadd('HighlightPur', '\%'.col('.').'v')
  elseif (a:color == 'White')
    call matchadd('HighlightWhi', '\%'.col('.').'v')
  endif
endfunction


function! HighlightLineWithColor(color)
  if (a:color == 'Red')
    call matchadd('HighlightRed', '\%'.line('.').'l')
  elseif (a:color == 'Yellow')
    call matchadd('HighlightYel', '\%'.line('.').'l')
  elseif (a:color == 'Cyan')
    call matchadd('HighlightCya', '\%'.line('.').'l')
  elseif (a:color == 'Green')
    call matchadd('HighlightGre', '\%'.line('.').'l')
  elseif (a:color == 'Purple')
    call matchadd('HighlightPur', '\%'.line('.').'l')
  elseif (a:color == 'White')
    call matchadd('HighlightWhi', '\%'.line('.').'l')
  endif
endfunction

function! HighlightColumnWithColor(color)
  if (a:color == 'Red')
    call matchadd('HighlightRed', '\%'.col('.').'v')
  elseif (a:color == 'Yellow')
    call matchadd('HighlightYel', '\%'.col('.').'v')
  elseif (a:color == 'Cyan')
    call matchadd('HighlightCya', '\%'.col('.').'v')
  elseif (a:color == 'Green')
    call matchadd('HighlightGre', '\%'.col('.').'v')
  elseif (a:color == 'Purple')
    call matchadd('HighlightPur', '\%'.col('.').'v')
  elseif (a:color == 'White')
    call matchadd('HighlightWhi', '\%'.col('.').'v')
  endif
endfunction

function! Switch()
  let filename = expand("%:t:r")
  let fileext = expand("%:e")
  if (fileext == "c")
    find %:t:r.h
  endif
  if (fileext == "cpp")
    find %:t:r.hpp
  endif
  if (fileext == "cc")
    find %:t:r.h
  endif
  if (fileext == "h")
    find %:t:r.c
  endif
  if (fileext == "hpp")
    find %:t:r.cpp
  endif
endfunction

function! TagIt()
  :silent !clear
  :silent !rm -frv tags 
  :silent !echo "Building tags..."
  :silent !find . -name '*.c' -o -name "*.cc" -o -name '*.cpp' -o -name '*.h' -o -name '*.hpp' > project.files
  :silent !cat project.files | ctags --sort=yes --c++-kinds=+p --fields=+iaS --extras=+q --language-force=C++ -f tags -L -
  :silent !echo "Done"
  :redraw!
endfunction

function! CompareStrings(s1,s2)
  if a:s1 ==# a:s2
    :echo 'EQUAL'
  else
    :echo 'NOT EQUAL !!!'
  endif
endfunction

function! ClearRegisters()
  let @a=''
  let @b=''
  let @c=''
  let @d=''
  let @e=''
  let @f=''
  let @g=''
  let @h=''
  let @i=''
  let @j=''
  let @k=''
  let @l=''
  let @m=''
  let @n=''
  let @o=''
  let @p=''
  let @q=''
  let @r=''
  let @s=''
  let @t=''
  let @u=''
  let @v=''
  let @w=''
  let @x=''
  let @y=''
  let @z=''
  let @0=''
  let @1=''
  let @2=''
  let @3=''
  let @4=''
  let @5=''
  let @6=''
  let @7=''
  let @8=''
  let @9=''
  let @+=''
  let @*=''
  let @/=''
endfunction

function! SetupCcls()
  silent !rm -frv Debug compile_commands.json CMakeCache.txt CMakeFiles ccls-cache
  silent !cmake . -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=YES
  silent ln -s Debug/compile_commands.json .
  if !filereadable('.ccls')
    silent !echo '\%compile_commands.json' > .ccls
    silent !echo '\%h -x' >> .ccls
    silent !echo '\%h c++-header' >> .ccls
  endif
  redraw!
endfunction

function! SetupJS()
  silent !rm -frv project.files tags
  silent !find . -name "*.js" > project.files
  silent !cp -R ~/.vim/usercfg/jsctags_options ./.ctags
  silent !cp -R ~/.vim/usercfg/tern-project-example ./.tern-project
  silent !cat project.files | ctags --options=./.ctags --sort=yes -f tags -L -
  redraw!
endfunction

function! GenerateHeaderGuard()
  let file_relative_path = expand('%:~:.:r')
  let fileext = expand("%:e")
  let guard = toupper(file_relative_path)
  let guard_separated = split(guard, "/")
  call add(guard_separated, toupper(fileext))
  let guard = join(guard_separated, "_")
  execute 'normal! gg2ddGddgg'
  put='#ifndef '.guard
  put='#define '.guard
  execute 'normal! 1GddG'
  put=''
  put='#endif /* '.guard.' */'
  execute 'normal! k'
endfunction

function! GitPanel()
  Flog
  Git
  wincmd w
  wincmd x
endfunction

function! RestoreMappings()
  call DisableGitMapping() 
  call EnableGenMapping() 
  if filereadable('./.vimrc') 
    source ./.vimrc 
  endif
endfunction

function! Hide(hidetype)
  if a:hidetype == 'all' 
    setlocal concealcursor=ni
    setlocal conceallevel=2
    syntax match Concealed '' conceal
  elseif a:hidetype == 'not_this'
    setlocal concealcursor=
    setlocal conceallevel=2
    syntax match Concealed '' conceal
  elseif a:hidetype == 'none'
    setlocal concealcursor=
    setlocal conceallevel=0
    syntax clear Concealed
  endif
endfunction

function! Foldmethod(fmethod)
  execute 'setlocal foldmethod='.a:fmethod
endfunction

function! Save(save_file)
  silent execute 'mksession! '.a:save_file
  if filereadable('./.vimrc')
    silent execute '!echo so ~/.vimrc >> '.a:save_file
    silent execute '!echo so ./.vimrc >> '.a:save_file
  endif
  redraw!
endfunction

function! Load(load_file)
  silent execute 'source '.a:load_file
  redraw!
endfunction

function! SwitchToNextFile()
  echo tabpagenr('$')
  if(tabpagenr('$') == 1)
    bnext
  else
    tabnext
  endif
endfunction

function! SwitchToPrevFile()
  echo tabpagenr('$')
  if(tabpagenr('$') == 1)
    bprevious
  else
    tabprevious
  endif
endfunction
