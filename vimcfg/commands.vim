" command! FormatToUnix :%s/\r$//<CR>
command! -nargs=1 GDB :ConqueGdb <f-args><CR>

command! -nargs=1 HColumn :call HighlightColumn(<f-args>)<CR>
command! -nargs=1 HLine   :call HighlightLine(<f-args>)<CR>

command! -nargs=1 WHD :execute "resize -" . <f-args>
command! -nargs=1 WHI :execute "resize +" . <f-args>
command! -nargs=1 WWD :execute "vertical resize -" . <f-args>
command! -nargs=1 WWI :execute "vertical resize +" . <f-args>

command! -nargs=1 Whereis :exec 'g/'.<f-args>.'/nu'

command! ClearRecentFilesList :execute ":silent !echo '' > " . MRU_File | :redraw!
command! ClearRegisters :call ClearRegisters()
command! Compare :call CompareStrings()
command! GetClipboardText :r!xsel -b
command! LockScroll :set scrollbind!<CR>
command! RecentFiles :MRU


command! -nargs=+ Compare call CompareStrings(<f-args>)
command! Format :call Format()
command! IsValid :call IsValid()
command! Validate :call Validate()

command! W :w !sudo tee %

command! GitPanel call GitPanel()

command! GenerateHeaderGuard call GenerateHeaderGuard()

command! -nargs=1 Save call Save(<f-args>)
command! -nargs=1 Load call Load(<f-args>)
