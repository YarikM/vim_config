call plug#begin('~/.vim/plugged')
Plug 'https://github.com/airblade/vim-gitgutter'
Plug 'https://github.com/alvan/vim-closetag.git'
Plug 'https://github.com/jreybert/vimagit'
Plug 'https://github.com/majutsushi/tagbar'
Plug 'https://github.com/rbong/vim-flog'
Plug 'https://github.com/rkulla/pydiction.git'
Plug 'https://github.com/scrooloose/nerdcommenter.git'
Plug 'https://github.com/tpope/vim-fugitive'
Plug 'https://github.com/tpope/vim-surround.git'
Plug 'https://github.com/sheerun/vim-polyglot.git'
Plug 'https://github.com/tmhedberg/matchit.git'
Plug 'https://github.com/neoclide/coc.nvim.git', { 'branch': 'release' }
Plug 'https://github.com/ternjs/tern_for_vim.git', { 'do': 'npm install' }
Plug 'https://github.com/ryanoasis/vim-devicons'
Plug 'https://github.com/vifm/vifm.vim.git'
Plug 'https://github.com/maksimr/vim-jsbeautify.git'
Plug 'https://github.com/junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'https://github.com/junegunn/fzf.vim'
Plug 'https://github.com/ap/vim-css-color.git'
Plug 'https://github.com/pappasam/coc-jedi', { 'do': 'yarn install --frozen-lockfile && yarn build', 'branch': 'main' }
Plug 'https://github.com/rhysd/vim-clang-format'
call plug#end()

colorscheme custom

source ~/.vim/usercfg/properties.vim
source ~/.vim/usercfg/ignorelist.vim
source ~/.vim/usercfg/pluginscfg.vim
source ~/.vim/usercfg/key_mappings/general_map.vim
source ~/.vim/usercfg/functions.vim
source ~/.vim/usercfg/commands.vim
source ~/.vim/usercfg/behavior.vim

function! StartUp(directory)
  wincmd p 
  ene 
  exe 'cd '.a:directory
  if filereadable('.vimrc') 
    source ./.vimrc 
  endif 
  set path=$PWD/**
endfunction

augroup ProjectDrawer
  autocmd!
  autocmd StdinReadPre * let s:std_in=1
  autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | call StartUp(argv()[0]) | endif
augroup END

" NOTES:
" WARNING!
" Slumlord plugin requiers vim version 8+ !!!
"
" USEFULL TIPS:
" use '%' to jump between closing bracers!!!
" use {i}gt to jump on i-tab where 'i' is number from 0 to INF
" use :tab all to open all files in argument list of vim in separate tabs
"
" Multiple replacement:
" search word with /
" type 'cgn' in normal mode
" type desired word
" go to normal mode
" press . to replace next word!
"
" Multiple block replacement
" Ctrl + V and select block
" Shift + I and enter the word to add 
" Press ESC and go to next line 
"
" Gblame
" Press 'o' on commit to open it in horizontal split
" Press <CR> to open it in current buffer. To exit press <C-S-^>
"
" Glog 
" Use Glog -- % OR Glog -10 -- % ( where 10 is number of commits to observe ) to see log for current file 
" :cw to get list of related commits
"
"
" Use . to repeat last deletion
" Use @: to repeat last command 
"
"
" Use mksession to save session in vim
" Use source <name-of-vim-session> to load session 
"
"
" Use 'zc' to close syntax fold 
" Use 'zo' to open syntax fold 
" Use 'zR' to unfold all
" Use 'za' to toggle fold
"
" In horisontal splitted windows
" Use <C-w>L to move window right from one above/below
" In vertical splitted windows 
" Use <C-w>J to move window to bottom
"
"
" For clang completion better use Ctrl-X Ctrl-O
" rather than Ctrl-X Ctrl-[
"
" Use `. to jump at last modified line in exact position of it
" Use Ctrl-O to jump backward through cursor positions
"
"
" In quickfix window use :[count]cn to move [count] steps down
" and :[count]cp to move [count] steps up
"
"
" ConqueGdb -- nice integration of gdb debugger
" Remember to link with -g option for cool tracking
" Use :ConqueGdb <binary_name> command to launch gdb like from terminal
" Vim will show where breakpoints are set if they are present
"
"
" Reference vimregex.com for regex usage in vim
"
" Surround HOW TO:
" Type 'csw' or 'csW' if you want to capture braces and then 
" type a char or expression which will embrace the selected word
" To delete press 'ds' and input quoting char(phrase)
" To cover line with \" use 'yss"'
"
"
" To count selected lines in visual mode press g and then C-g
"
"
"
" Flog HOW TO:
" use 'u' to update
" use y<C-g> to copy commit hash in system clipboard
"
" Terminal HOW TO:
" There is possibility to use terminal internaly in vim.
" It is provided with :terminal command
" In case you want use terminal in new tab like :tab terminal
" remember, that escaping tab will only be available with <C-w>:tabn(p)
" command
"
