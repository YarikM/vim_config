set background=dark
highlight clear
if exists("syntax_on")
  syntax reset
endif

let g:colors_name = "custom"

highlight clear SignColumn       

highlight Constant         cterm=none         ctermfg=202     
highlight CursorColumn     cterm=none													ctermbg=0
highlight CursorLine       cterm=bold,italic									
highlight CursorLineNr     cterm=bold,italic									ctermbg=232        
highlight DiffAdd          cterm=bold         ctermfg=10      ctermbg=17 
highlight DiffChange       cterm=bold         ctermfg=10      ctermbg=17 
highlight DiffDelete       cterm=bold         ctermfg=10      ctermbg=17 
highlight DiffText         cterm=bold         ctermfg=10      ctermbg=88 
highlight Error            cterm=reverse      ctermfg=15      ctermbg=9 
highlight Folded           cterm=none         ctermfg=82      ctermbg=237
highlight Function         cterm=none         ctermfg=5       
highlight CustomCyan       cterm=bold         ctermfg=0       ctermbg=123   
highlight CustomGreen      cterm=bold         ctermfg=0       ctermbg=118   
highlight CustomPurple     cterm=bold         ctermfg=0       ctermbg=5     
highlight CustomRed        cterm=bold         ctermfg=15      ctermbg=197   
highlight Identifier       cterm=bold         ctermfg=40      
highlight Ignore           cterm=none         ctermfg=0       
highlight LineNr           cterm=italic       ctermfg=33      
highlight LineNrBelow      cterm=italic       ctermfg=46      
highlight MatchParen       cterm=bold         ctermfg=52      ctermbg=2
highlight Normal           cterm=none         ctermfg=248     
highlight Operator         cterm=none         ctermfg=15      
highlight Pmenu            cterm=none         ctermfg=250     ctermbg=235
highlight PmenuSel         cterm=none         ctermfg=45      ctermbg=235
highlight PreProc          cterm=none         ctermfg=81      
highlight Repeat           cterm=none         ctermfg=190     
highlight Search           cterm=bold         ctermfg=0       ctermbg=81
highlight SignColumn       cterm=none         ctermfg=190
highlight Special          cterm=bold         ctermfg=5       
highlight Statement        cterm=none         ctermfg=190     
highlight String           cterm=none         ctermfg=172     
highlight Todo             cterm=standout     ctermfg=0       
highlight Type             cterm=none         ctermfg=84      
highlight VertSplit        cterm=none         ctermfg=154     
highlight comment          cterm=italic       ctermfg=28      
highlight WildMenu         cterm=bold         ctermfg=0   ctermbg=220

" Status line groups
highlight link StatusLineNC StatusLine
highlight StatusLine              cterm=bold				ctermbg=0
highlight StatusLineNotSavedFlag  cterm=bold			  ctermfg=196
highlight StatusLineFileFlags     cterm=bold				ctermfg=141
highlight StatusLineFileName      cterm=italic			ctermfg=48  ctermbg=235
highlight StatusLineByteInfo      cterm=none				ctermfg=220
highlight StatusLineLineInfo      cterm=none				ctermfg=123
highlight StatusLineTabInfo       cterm=none				ctermfg=112
highlight StatusLineROInfo        cterm=none				ctermfg=199
highlight StatusLineCurrentTag    cterm=none				ctermfg=75
highlight StatusLineCurrentBranch cterm=none				ctermfg=184


" ===== HIGHLIGHT LINKS ======
" Common groups that link to default highlighting.
" You can specify other highlighting easily.
highlight link   Boolean         Constant
highlight link   Character       Constant
highlight link   Conditional     Statement
highlight link   Debug           Special
highlight link   Define          PreProc
highlight link   Delimiter       Special
highlight link   Exception       Statement
highlight link   Float           Constant
highlight link   Include         PreProc
highlight link   Keyword         Statement
highlight link   Label           Statement
highlight link   Macro           PreProc
highlight link   Number          Constant
highlight link   PreCondit       PreProc
highlight link   SpecialChar     Special
highlight link   SpecialComment  Special
highlight link   StorageClass    Type
highlight link   Structure       Type
highlight link   Tag             Special
highlight link   Typedef         Type
