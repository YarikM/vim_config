#! /bin/bash

LINUX_CONFIG_DIR="${PWD}"
VIM_CONFIG="${LINUX_CONFIG_DIR}/vimcfg"

# Delete old configurations
if [ -d "${HOME}/.vim" ]; then
  rm -frv "${HOME}/.vim"
fi

if [ -d "${HOME}/.config/coc" ]; then
  rm -frv "${HOME}/.config/coc"
fi

if [ -f "${HOME}/.vimrc" ]; then
  rm -fv "${HOME}/.vimrc"
fi

# Prepare dirs and paths
mkdir -p   ${HOME}/.vim/
mkdir -p   ${HOME}/.vim/autoload
mkdir -p   ${HOME}/.vim/plugged 
mkdir -p   ${HOME}/.vim/colors
mkdir -p   ${HOME}/.vim/syntax
mkdir -p   ${HOME}/.vim/tags
mkdir -p   ${HOME}/.vim/usercfg
mkdir -p   ${HOME}/.config/coc

# install plugin manager
curl -fLo  ${HOME}/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

ln -sf ${VIM_CONFIG}/vimrc          ${HOME}/.vimrc 
ln -sf ${VIM_CONFIG}/custom.vim     ${HOME}/.vim/colors/custom.vim

for cfg_file in "ignorelist" "properties" "commands" "functions" "pluginscfg" "behavior"; do
  ln -sf ${VIM_CONFIG}/${cfg_file}.vim  ${HOME}/.vim/usercfg/${cfg_file}.vim
done

for cfg_dir in "key_mappings"; do
  ln -sf ${VIM_CONFIG}/${cfg_dir} ${HOME}/.vim/usercfg/${cfg_dir}
done

vim -c "PlugInstall" -c "qa!"
vim -c "CocInstall -sync coc-clangd coc-lua coc-json coc-tsserver coc-jedi coc-java coc-css coc-html coc-cmake coc-sh coc-vimlsp coc-vetur coc-explorer" -c "qa!" 

for syntax_file in "python" "puml" "log"; do 
  find ${HOME}/.vim/ -name "${syntax_file}.vim" -delete
  ln -sf ${VIM_CONFIG}/syntax/${syntax_file}.vim ${HOME}/.vim/syntax/${syntax_file}.vim
done

pushd $VIM_CONFIG/helpers
  python3 coc-config-gen.py 
  mv $VIM_CONFIG/helpers/coc-settings.json ${HOME}/.vim/coc-settings.json
popd

echo "Enjoy!"
